package learn_golang_modules

func SayHello(name string) string {
	return "Hello, "+ name
}

func SayGoodbye(name string) string {
	return "Good Bye "+ name
}

func SayGoodNight(name string) string {
	return "Good Night"+ name
}

func SayGoodMorning(name string) string {
	return "Good Morning"+ name
}